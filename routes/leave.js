const express = require('express')
const router = express.Router()
const leaveUtil = require('../controller/leave_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.post('/insert_leave',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    leaveUtil.insert_leave(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result})
    })


module.exports = router