const express = require('express')
const router = express.Router()
const userUtil = require('../controller/benefit_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.post('/insert_benefit',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.insert_benefit(),
    function (req, res) {
        res.status(200).json({ 'success': true, ben_type: req.ben_type, add_empID: req.add_empID, empID: req.empID, status: req.status, amount: req.amount, ben_date_receipt: req.ben_date_receipt, ben_date_request: req.ben_date_request })
        //โชว์ข้อมูล
    })


module.exports = router