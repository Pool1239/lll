const express = require('express')
const router = express.Router()
const leaveTypeUtil = require('../controller/leave_type_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.get('/get_leave_type',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    leaveTypeUtil.get_leave_type(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.get('/get_inner_join',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    leaveTypeUtil.get_inner_join(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })


    router.get('/get_leave_type/:id',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    leaveTypeUtil.get_leave_type_from_id(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.post('/get_leave_type',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    leaveTypeUtil.get_leave_type_by_id(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    
module.exports = router