const express = require('express')
const router = express.Router()
const userUtil = require('../controller/update_lea_quest_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.post('/update_leave_request',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_leave_request(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.post('/update_leave_request_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_leave_request_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })    

module.exports = router