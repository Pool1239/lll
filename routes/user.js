const express = require('express')
const router = express.Router()
const userUtil = require('../controller/user_controller')
const authUtil = require('../controller/auth_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')
var path = require('path')

router.post('/insert_user',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.insert_user(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
router.post('/web_login',
    authUtil.user_login(),
    function (req, res) {
        req.session.token = req.tokenWeb
        console.log(req.session.token)
        res.status(200).json({ 'success': true, result: req.result })
    })

router.post('/mobile_login',
    authUtil.user_login(),
    function (req, res) {
        res.status(200).json({ 'success': true, userName: req.userName, emp_name: req.emp_name, email: req.email, role: req.role, role_id: req.role_id, pro_name: req.pro_name, token: req.token })
    })

router.get('/user_profiles',
    validateUtil.validate_token(),
    function (req, res) {
        console.log(req.user_create_id)
        require("fs").readFile(path.join(__dirname, '..', 'user_profiles', req.user_create_id + ".png"), (err, data) => {
            console.log("filetest", req.user_create_id)
            //error handle
            if (err) {
                console.log('xxx')
                res.sendFile(path.join(__dirname, '..', 'user_profiles', "default.png"))


            } else {
                // console.log("file", imgSrcString)
                res.sendFile(path.join(__dirname, '..', 'user_profiles', req.user_create_id + ".png"))
            }


        })

    })

module.exports = router

