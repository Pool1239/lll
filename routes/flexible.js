const express = require('express')
const router = express.Router()
const flexibleUtil = require('../controller/flexible_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.post('/insert_flexible',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    flexibleUtil.insert_flexible(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.post('/insert_profile',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    flexibleUtil.insert_profile(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })


module.exports = router