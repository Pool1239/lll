const express = require('express')
const router = express.Router()
const userUtil = require('../controller/leave_request_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.post('/insert_leave_request',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.insert_leave_request(),
    function (req, res) {
        res.status(200).json({ 'success': true, lea_reason: req.lea_reason })
    })


module.exports = router