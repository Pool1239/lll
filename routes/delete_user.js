const express = require('express')
const router = express.Router()
const userUtil = require('../controller/delete_user_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.post('/delete_user',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.delete_user(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })


module.exports = router