const express = require('express')
const router = express.Router()
const userUtil = require('../controller/flexible_benefit_con')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')
var path = require('path')

router.get('/show_benefit',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_benefit(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.get('/show_benefit_2',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_benefit_2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

    router.post('/show_benefit_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_benefit_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

    router.post('/update_benefit',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_benefit(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

    router.post('/update_benefit_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_benefit_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

    router.get('/bill/:id',
    validateUtil.validate_token(),
    function (req, res) {
        console.log(path.join(__dirname,'..','bill',req.params.id+".png"))
        res.sendFile(path.join(__dirname,'..','bill',req.params.id+".png"))
    })

   

module.exports = router