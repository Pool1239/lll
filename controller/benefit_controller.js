var db = require('../connection/dbConnection')
var moment = require('moment-timezone')

exports.insert_benefit = function () {
    return function (req, res, next) {
        var registerInfo = {
            ben_type: req.body.ben_type,
            ben_date_request : moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss") ,
            ben_date_receipt : moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss") ,
            amount : req.body.amount,
            status : req.body.status,
            empID : req.body.empID,
            add_empID : req.body.add_empID, 
        }

        db.query("INSERT INTO benefit (ben_type,ben_date_request,ben_date_receipt,amount,status,empID,add_empID) VALUES\
        ('"+ registerInfo.ben_type + "', '" + registerInfo.ben_date_request + "', '" + registerInfo.ben_date_receipt + "', '" + registerInfo.amount
        + "', '" + registerInfo.status + "', '"  + registerInfo.empID + "', '" + registerInfo.add_empID + "')", function (err, result) {
                     if (err) throw err;
                     req.ben_type = registerInfo.ben_type
                     next()
                 })
    }
}