var db = require('../connection/dbConnection')
var moment = require('moment-timezone')

exports.insert_leave_request = function () {
    return function (req, res, next) {
        var registerInfo = {
            lea_type: req.body.lea_type,
            lea_day : req.body.lea_day,
            date_start : moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss") ,
            date_end : moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss") ,
            lea_reason : req.body.lea_reason,
            status : req.body.status,
            empID : req.body.empID,
            add_empID : req.body.add_empID,
        }

        db.query("INSERT INTO leave_request (lea_type,lea_day,date_start,date_end,lea_reason,status,empID,add_empID) VALUES\
        ('"+ registerInfo.lea_type + "', '" + registerInfo.lea_day + "', '" + registerInfo.date_start + "', '" + registerInfo.date_end
        + "', '" + registerInfo.lea_reason + "', '"  + registerInfo.status + "', '" + registerInfo.empID + "', '" + registerInfo.add_empID + "')", function (err, result) {
                     if (err) throw err;
                     req.lea_reason = registerInfo.lea_reason
                     next()
                 })
    }
}
