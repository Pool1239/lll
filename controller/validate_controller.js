var jsonwebToken = require('jsonwebtoken')

exports.validate_token = function () {
    return function (req, res, next) {
        console.log(req.session)
        if (req.session.token) {
            jsonwebToken.verify(req.session.token, "humanresource", (err, decode) => {
                if (err) {
                    res.status(201).json({ success: false, message: 'token not found', error: err })
                } else {
                    req.user_create_id = decode.id
                    console.log(req.session.token)
                    next()
                }
            })
        }
        else if (Boolean(req.headers['authorization'])) {
            jsonwebToken.verify(req.headers.authorization, "humanresource", (err, decode) => {
                if (err) {
                    res.status(201).json({ success: false, message: 'token not found', error: err })
                } else {
                    req.user_create_id = decode.id
                    next()
                }
            })
        }
    }
} 