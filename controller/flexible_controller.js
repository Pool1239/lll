
var db = require('../connection/dbConnection')
var moment = require('moment-timezone')

exports.insert_flexible = function () {
    return function (req, res, next) {
        var registerInfo = {
            ben_type: req.body.ben_type,
            ben_date_request:  moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss"),
            ben_date_receipt: req.body.ben_date_receipt,
            amount: req.body.amount,
            status: '1',
            empID: req.body.empID,
            add_empID: '2',
            flexible_created: moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss"),
        }

        db.query("INSERT INTO benefit (ben_type,ben_date_request,ben_date_receipt,amount,status,empID,add_empID,flexible_created) VALUES\
        ('"+ registerInfo.ben_type + "', '" + registerInfo.ben_date_request + "', '" + registerInfo.ben_date_receipt + "', '" + registerInfo.amount + "', '" + registerInfo.status + "', '" + registerInfo.empID + "', '" + registerInfo.add_empID + "', '" + registerInfo.flexible_created + "')", function (err, result) {
                if (err) throw err;
                req.result = registerInfo.result
                
                console.log(result)
                console.log(req.body.image)
                var base64Data = req.body.image.split(',')[1];
                require("fs").writeFile("./bill/"+result.insertId+".png",base64Data,'base64',function (err){
                    console.log
                    next (); 
                })
               
            })
    }
}
exports.insert_profile = function () {
    return function (req, res, next) {
                var base64Data = req.body.image.split(',')[1];
                require("fs").writeFile( "./user_profiles/" + req.body.empID + ".png" , base64Data , 'base64' ,function (err){
                    console.log
                    next (); 
                })
    }
}
