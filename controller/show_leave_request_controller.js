// var errorMessages = require('../const/error_messages')
var db = require('../connection/dbConnection')
// var constance = require('../const/constance')
// var moment = require('moment-timezone')

exports.show_leave_request = function () {
    return function (req, res, next) {
        db.query('SELECT l.leaID as leaveID,e.empID as Empid,e.emp_name,e.emp_last,leaName_day,leaName_type,date_start,date_end,sta_name,ed.userName as nameuser,ed.empID as addmin,l.lea_reason,r.rol_name,l.date_created,DATEDIFF(date_end,date_start) as datediff,TIMEDIFF(date_end,date_start) as timediff FROM leave_request l join employee e on l.empID = e.empID join employee ed on l.add_empID = ed.empID join lea_day ld on l.lea_day=ld.leaID_day join lea_type lt on l.lea_type=lt.leaID_type join status s on l.status=s.staID join role r on e.role=r.rolID order by e.empID asc ', function (err, result) {
            
            req.result = result.map(el => ({ ...el, full_name: el.emp_name +' ' + el.emp_last,full_day: el.datediff + ' day. ' + el.timediff + ' hrs.' }));
            next();
            
        })
    }
}