// var errorMessages = require('../const/error_messages')
var db = require('../connection/dbConnection')
// var constance = require('../const/constance')
// var moment = require('moment-timezone')

exports.show_user = function () {
    return function (req, res, next) {
        db.query('SELECT * FROM employee e join pro_period p on e.pro_period = p.proID join role r on e.role = r.rolid  order by empID asc;', function (err, result) {
            console.log(result)
            req.result = result.map(el => ({ ...el, full_name: el.emp_name +' ' + el.emp_last, admin_email: result.find(e => e.empID === el.user_create_id).email }) );
            next();
            
        })
    }
}

exports.show_user2 = function () {
    return function (req, res, next) {
        db.query('SELECT count(empID) as sumid FROM hr_web.employee;', function (err, result) {
            console.log(result)
            req.result = result;
            next();
            
        })
    }
}