var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var port = 3001;
var version = '/api/v1/';
var moment = require('moment')
var logger = require('morgan')
var schedule = require('node-schedule')
var rimraf = require('rimraf')
var fs = require('fs');
const cookie = require('cookie-session') 
const access = require('access-control')
const core = access({maxAge: '8 hour', credentials: true, origin: true})

var mm = moment()
var date = mm.utc().format('DD-MM-YYYY')
var time = mm.utc().format('HH: mm: ss')

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));


// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization, X-Access-Token')
  res.setHeader('Access-Control-Allow-Credentials', true)

  // Pass to next layer of middleware
  next()
});

app.use(cookie({ 
  name: 'hr_session', 
  keys:['hr'], 
  maxAge: 8 *60 *60*1000, 
  cookie:{ 
    httpOnly: true, 
    secure: false 
  }  
})) 

// Service
var user = require('./routes/user')
var benefit = require('./routes/benefit')
var leave_request = require('./routes/leave_request')
var show_user = require('./routes/show_user')
var delete_user = require('./routes/delete_user')
var show_leave_request = require('./routes/show_leave_request')
var my_leave_request = require('./routes/my_leave_request')
var leave_type = require('./routes/leave_type')
var show_request = require('./routes/show_request')
var update_leave_request = require('./routes/update_leave_request')
var show_username = require('./routes/show_username_my_leave_request')
var flexible_benefit = require('./routes/flexible_benefit')
var leave = require('./routes/leave')
var flexible = require('./routes/flexible')
// var update_benefit_2 = require('./routes/update_benefit_2')

schedule.scheduleJob('59 59 23 * * *', () => {
  let date = moment().utcOffset('+07:00').format('YYYYMMDD')
  rimraf(`./public/${date}`, () => console.log(`remove folder: ${date}`))
})
schedule.scheduleJob('01 00 00 * * *', () => {
  let date = moment().utcOffset('+07:00').format('YYYYMMDD')
  var dir = `./public/${date}`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    console.log(`create folder: ${date}`)
  }
})

app.use(logger('dev'))
var accessLogStream = fs.createWriteStream(`${__dirname}/logs/${date}.log`, { flags: 'a' })
var configlog = `[${time}] [ip]: :remote-addr :remote-user [method]: :method [url]: :url HTTP/:http-version [status]: :status [response-time]: :response-time ms [client]: :user-agent`
app.use(logger(configlog, { stream: accessLogStream }))

app.use(version, user)
app.use(version, benefit)
app.use(version , leave_request)
app.use(version , show_user )
app.use(version , delete_user)
app.use(version , show_leave_request)
app.use(version , my_leave_request)
app.use(version , leave_type)
app.use(version , show_request)
app.use(version , update_leave_request)
app.use(version , show_username)
app.use(version , flexible_benefit)
app.use(version , leave)
app.use(version , flexible)
// app.use(version , update_benefit_2)

var server = app.listen(port, function () {
  console.log('Server is running port: ' + port);
});